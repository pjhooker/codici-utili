﻿--visualizza le coordinate in diversi EPSG(SR)
SELECT
  'epsg4326' as epsg,
  st_asgeojson(geom) as geojson
FROM (
  SELECT ST_SetSRID(ST_Point(10.238250,44.640819),4326) as geom
) as foo
UNION ALL
SELECT
  'epsg32632' as epsg,
  st_asgeojson(st_transform(geom,32632)) as geojson
FROM (
  SELECT ST_SetSRID(ST_Point(10.238250,44.640819),4326) as geom
) as foo
UNION ALL
SELECT
  'epsg3857' as epsg,
  st_asgeojson(st_transform(geom,3857)) as geojson
FROM (
  SELECT ST_SetSRID(ST_Point(10.238250,44.640819),4326) as geom
) as foo
UNION ALL
SELECT
  'epsg3003' as epsg,
  st_asgeojson(st_transform(geom,3003)) as geojson
FROM (
  SELECT ST_SetSRID(ST_Point(10.238250,44.640819),4326) as geom
) as foo